# 2020 Guyanese general election

    start_date: 2020-03-02
    end_date: 2020-03-02
    source: https://guyanaresults.com/
    wikipedia: https://en.wikipedia.org/wiki/2020_Guyanese_general_election

**The candidates file does not specify which candidates should be active. If you want any candidates to show up on Open Elections, add a column with the header `active` and add `TRUE` to any candidate you want to include.**